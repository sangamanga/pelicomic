# Start with the lightweight alpine base
FROM alpine:3.5

# Add the dependencies for the build that are in the repo
RUN apk add --no-cache python-dev py2-pip py2-pillow py2-lxml git
# Add the dependencies for the build that are not in the repo
RUN LIBRARY_PATH=/lib:/usr/lib /bin/sh -c "pip2 install gilt-python pelican bs4"

PeliComic
=========
PeliComic is a kind of bolt-on prosthesis designed to make Pelican_ a better
platform for hosting statically generated webcomics. While Pelican is an
excellent tool, it's clearly designed with blogs in mind (with a feature set
to match). A tight integration between theming and plugin functionality is
necessary for a static site generator to acheive the kind of functionality
many people expect from their webcomics these days.

.. _Pelican: https://blog.getpelican.com/

PeliComic really requires knowledge of the following tools to be anything
approaching serviceable right now:

- git_
- reStructuredText_
- Pelican
- Python_ (and virtualenv_)

.. _git: https://git-scm.com/
.. _reStructuredText: http://docutils.sourceforge.net/
.. _Python: https://www.python.org/
.. _virtualenv: https://virtualenv.pypa.io/en/stable/

Dependencies
~~~~~~~~~~~~
- gilt
- pelican
- py-lxml
- py-pillow
- bs4

Optional:

- libsass
- awscli

How
~~~
PeliComic makes use of gilt_ in order to layer its data on top of your Pelican
content. While this seems messy, it's a lot less complex to the novice than
maintaining multiple git submodules and merging upstream regularly. If you
arrive at a better selection however, we'd be excited to hear it.

.. _gilt: https://github.com/metacloud/gilt

Your repository will likely look somewhat like the following:

.. code-block:: shell

  .gitignore # a file to keep unwanted files out of your git commits
  gilt.yml # the gilt layering definition
  pelicanconf.py # the 'base' configuration for your comic
  publishconf.py # the 'live website' configuration
  content/ # your actual comic content
  content/pages # static pages for your webcomic
  content/strips # the .rst files for your comic pages
  content/images # the images for your webcomic
  .gitlab-ci.yml # optional, autobuilds a GitLab pages site from your comic

For everything but the :code:`content/` folder there are examples in the
:code:`examples` folder of this repository. In order to leverage PeliComic,
once you've created a :code:`gilt.yml` in your repository you will run
:code:`gilt overlay` which will pull in the necessary external files so that
your compic can be built by Pelican with all of the added PeliComic flavor.

For more detailed usage documentation please see the Wiki_.

.. _Wiki: https://gitlab.com/sangamanga/pelicomic/wikis/home

"""
figure_img_alt_to_title plugin.

Designed for webcomics. Repurposes ReST figure directive to be for main comic
panel by making alt attribute in figure img divs be title attribute instead. It
also gives img attributes inside a figure a class 'figure-img' so that they're
easier to modify and style independently.
"""

from __future__ import unicode_literals
from pelican import signals
from bs4 import BeautifulSoup


def content_object_init(page):

    if page._content is not None:
        content = page._content
        parser = 'lxml' #context.get("IMAGE_PROCESS_PARSER", "html.parser")
        soup = BeautifulSoup(content, parser)

        for img in soup(['img', 'object']):
            if img.find_parent('div', 'figure'):
                if img.name == 'img':
                    if img['alt']:
                        # turn alt into title
                        img['title'] = img['alt']
                        img['alt'] = ''
                    # add figure-img class to img for easier handling
                    # by plugins that don't know about figures
                    img['class'] = 'figure-image image-process-figure-image'

        page._content = soup.decode()


def register():
    signals.content_object_init.connect(content_object_init)

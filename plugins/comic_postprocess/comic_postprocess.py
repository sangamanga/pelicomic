# -*- coding: UTF-8 -*-
"""
figure_img_alt_to_title plugin.

Designed for webcomics. Repurposes ReST figure directive to be for main comic
panel by making alt attribute in figure img divs be title attribute instead. It
also gives img attributes inside a figure a class 'figure-img' so that they're
easier to modify and style independently.
"""

from __future__ import unicode_literals
from pelican import signals
from bs4 import BeautifulSoup
from publishconf import SITEURL

def rewrite_page(path, context):
    # Set default value for 'IMAGE_PROCESS_DIR'.
    with open(path, 'r+') as output_file:
        res = rewrite_content(output_file, context)
        output_file.seek(0)
        output_file.truncate()
        output_file.write(res.encode('utf-8'))


def rewrite_content(filedata, context):

    parser = context.get("IMAGE_PROCESS_PARSER", "html.parser")
    soup = BeautifulSoup(filedata, parser)

    pagination = soup.find('div', class_='pagination')

    if pagination is not None and \
        len(pagination.contents) > 1 and \
        pagination.contents[-2].name is 'a':
        next_page = pagination.contents[-2]['href']
    else:
        next_page = "#"

    figure_image = soup.find('img', class_='figure-image')

    if figure_image is not None:
        original_image_uri = figure_image['src'].split('/')
        original_image_name = original_image_uri[-1]
        derivatives_position = original_image_uri.index('derivatives')
        munged_image_uri = original_image_uri[:derivatives_position]
        munged_image_uri.append(original_image_name)
        formatted_image_uri = '/'.join(munged_image_uri)
        original_link = soup.new_string(u'\U0001f50d'.encode('utf-8'))
        figure_image.insert_after(original_link)
        original_link.wrap(soup.new_tag('a', href=formatted_image_uri, **{'class':'original-link'}))
        figure_image.wrap(soup.new_tag('a', href=next_page))
        # This is for fixing up the twitter card placeholder from the template with real text
        # we can't do the image inside the template because we have no clue what it'll be
        twittercard_image = soup.find('meta', attrs={'name':'twitter:image'})
        twittercard_image['content'] = SITEURL + '/' + formatted_image_uri

    return soup.decode()

def register():
    signals.content_written.connect(rewrite_page)

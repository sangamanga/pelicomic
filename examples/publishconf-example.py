#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

SITEURL = 'foo'
RELATIVE_URLS = False


FEED_DOMAIN = SITEURL
FEED_ALL_ATOM = None
FEED_ALL_RSS = None
CATEGORY_FEED_ATOM = 'feeds/%s.atom.xml'
CATEGORY_FEED_RSS = 'feeds/%s.rss.xml'
FEED_MAX_ITEMS = 50

DELETE_OUTPUT_DIRECTORY = True

# Donations
DONATE_BUTTON_TARGET = 'foo'

#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'foo'
SITENAME = 'bar'
SITESUBTITLE = 'baz'
SITEURL = 'http://localhost:8000'
THEME = 'themes/herpaderp'
PLUGIN_PATHS = ['plugins']
PLUGINS = ['neighbors',
           'comic_preprocess',
           'image_process',
           'comic_postprocess']
PATH = 'content'
METADATA = "foo"

TIMEZONE = 'America/Los_Angeles'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Pages
DIRECT_TEMPLATES = ('archives', '404', 'index')
DISPLAY_PAGES_ON_MENU = True
ARCHIVES_URL = 'archives/'
ARCHIVES_SAVE_AS = 'archives/index.html'

MENUITEMS = (('Home', ''),
             ('Archive', 'archives/'))

# Blogroll (not used in this theme)
# LINKS = (('Pelican', 'http://getpelican.com/'),)

# Social widget (displays under navigation buttons) (not currently in use)
SOCIAL = None

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

# ADVERTISEMENTS
PW_BOTTOM_ADBOX_CODE = '''insert literal HTML for projectwonderful ad box'''

# Donations
DONATE_BUTTON_TARGET = 'https://foo.com'

# AddThis
ADDTHIS_ID = "foo"

# Statuscake
STATUSCAKE_ID = "foo"

# COMICROCKET
COMICROCKET_ANALYTICS_KEY = "foo"
COMICROCKET_PROMO_BANNER = False

# Flattr
FLATTR_ID = "foo"
FLATTR_SCRIPT_ID = "bar"

# handles the resizing for figure images
IMAGE_PROCESS_PARSER = "lxml"
IMAGE_PROCESS = {
  'figure-image': {
    'type': 'responsive-image',
    'sizes': '80vw',
    'srcset': [('320w', ["scale_in 320 10000 True"]),
               ('640w', ["scale_in 640 10000 True"]),
               ('1280w', ["scale_in 1280 10000 True"])],
    'default': ('320w')
    }
}

# For the header catchphrase generation script
THEME_CATCHPHRASES = [
    "foo",
    "bar",
    "baz",
]
